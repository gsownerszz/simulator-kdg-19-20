package be.kdg.sa.simulator.ride.vehicle;

public class VehicleDTO {
    private Long vehicleID;
    private float xCoord;
    private float yCoord;
    private VehicleType type;
    public VehicleDTO() {
    }

    public Long getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(Long vehicleID) {
        this.vehicleID = vehicleID;
    }

    public float getxCoord() {
        return xCoord;
    }

    public void setxCoord(float xCoord) {
        this.xCoord = xCoord;
    }

    public float getyCoord() {
        return yCoord;
    }

    public void setyCoord(float yCoord) {
        this.yCoord = yCoord;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Vehicle: " + vehicleID + " of type: " + type + " is at (" + xCoord +","+yCoord+")";
    }
}
