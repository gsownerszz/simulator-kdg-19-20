package be.kdg.sa.simulator.ride.vehicle;

public enum VehicleType {
    UNKNOWN,
    VELO_BIKE,
    VELO_E_BIKE,
    ROAMING_E_STEP,
    ROAMING_SCOOTER
}
