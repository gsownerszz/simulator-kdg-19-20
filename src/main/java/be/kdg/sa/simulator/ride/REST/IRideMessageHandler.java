package be.kdg.sa.simulator.ride.REST;

import be.kdg.sa.simulator.ride.vehicle.VehicleDTO;
import be.kdg.sa.simulator.ride.vehicle.VehicleType;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IRideMessageHandler {
    CompletableFuture<Long> unlockStationVehicle(Long userID, Long stationID) throws Exception;

    CompletableFuture<Boolean> unlockFreeVehicle(Long userID, Long vehicleID)throws Exception;

    void sendLocationOfFreeVehicle(Date timeStamp, Long vehicleID, float xCoord, float yCoord);

    CompletableFuture<List<Long>> getFreeLocks(Long stationID)throws Exception;

    void lockStationVehicle(Long userID, Long stationID)throws Exception;

    CompletableFuture<Boolean> lockFreeVehicle(Long userID, Long vehicleID) throws Exception;

    CompletableFuture<VehicleDTO> findNearestFreeVehicle(float xCoord, float yCoord, VehicleType vehicleType) throws Exception;
}
