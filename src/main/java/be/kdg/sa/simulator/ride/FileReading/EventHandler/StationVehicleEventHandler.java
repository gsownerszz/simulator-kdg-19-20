package be.kdg.sa.simulator.ride.FileReading.EventHandler;

import be.kdg.sa.simulator.ride.FileReading.RideEvents;
import be.kdg.sa.simulator.ride.services.IRideService;

public class StationVehicleEventHandler implements IRideEventHandler {
    private Long stationId;
    private Long userId;
    private Long delay;
    private final IRideService rideService;
    public StationVehicleEventHandler(IRideService rideService){
        this.rideService = rideService;
    }

    private void mapEventData(RideEvents event, String[] eventData) throws Exception{
        if (event == RideEvents.LOCK || event == RideEvents.UNLOCK){
            userId = Long.parseLong(eventData[0]);
            stationId = Long.parseLong(eventData[1]);
            delay = Long.parseLong(eventData[2]);
        }
    }
    @Override
    public void doEvent(RideEvents event, String[] eventData) throws Exception{
        mapEventData(event,eventData);
        try {
            Thread.sleep(delay);
                if (event == RideEvents.UNLOCK){
                    rideService.unlockStationVehicle(userId, stationId);
                }else if (event == RideEvents.LOCK){
                    Long freeLock =  rideService.getFreeLocks(stationId).get(0);
                    rideService.lockStationVehicle(userId,freeLock);
                }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
