package be.kdg.sa.simulator.ride.FileReading.EventHandler;

import be.kdg.sa.simulator.ride.FileReading.RideEvents;
import org.springframework.stereotype.Component;

@Component
public class RideEventHandler {
    private IRideEventHandler rideEventHandler;
    public void setRideEventHandler(IRideEventHandler rideEventHandler){
        this.rideEventHandler = rideEventHandler;
    }
    public void doEvent(RideEvents event, String[] eventData) throws Exception{
        rideEventHandler.doEvent(event, eventData);
    }
}
