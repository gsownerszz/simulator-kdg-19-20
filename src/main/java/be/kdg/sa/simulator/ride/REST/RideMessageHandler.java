package be.kdg.sa.simulator.ride.REST;

import be.kdg.sa.simulator.ride.vehicle.VehicleDTO;
import be.kdg.sa.simulator.ride.vehicle.VehicleStateDTO;
import be.kdg.sa.simulator.ride.vehicle.VehicleType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.scheduling.annotation.Async;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class RideMessageHandler implements IRideMessageHandler {
    private final Gson gson;
    private final HttpClient client;
    private final String API_URL = "http://localhost:9091/api/";
    public RideMessageHandler(Gson gson, HttpClient client){
        this.client = client;
        this.gson = gson;
    }

    @Override
    @Async
    public CompletableFuture<Long> unlockStationVehicle(Long userID, Long stationID) throws Exception{
        try {
            VehicleStateDTO dto = new VehicleStateDTO();
            dto.setUserID(userID);
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/stations/unlock/" + stationID.toString()))
                    .PUT(HttpRequest.BodyPublishers.ofString(gson.toJson(userID)))
                    .build();

            CompletableFuture<Long> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> { //System.out.println("Requested unlockStationVehicle and got response:" +response.body());
                        return response; } )
                    .thenApply(HttpResponse::body)
                    .thenApply(response -> gson.fromJson(response,Long.TYPE));
           return CompletableFuture.completedFuture(future.get());

        }catch (Exception e){
            throw  new Exception("Ride Service is not available");
        }

    }
    @Override
    @Async
    public CompletableFuture<Boolean> unlockFreeVehicle(Long userID, Long vehicleID)throws Exception{
        try {
            VehicleStateDTO dto = new VehicleStateDTO();
            dto.setUserID(userID);
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/free_vehicles/unlock/" + vehicleID.toString()))
                    .PUT(HttpRequest.BodyPublishers.ofString(gson.toJson(userID)))
                    .build();



            CompletableFuture<Boolean> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> { //System.out.println("Requested unlockStationVehicle and got response:" +response.body());
                        return response; } )
                    .thenApply(HttpResponse::body)
                    .thenApply(response -> gson.fromJson(response,Boolean.TYPE));
            return CompletableFuture.completedFuture(future.get());

        }catch (Exception e){
            throw  new Exception("Ride Service is not available");
        }
    }
    @Override
    @Async
    public void sendLocationOfFreeVehicle(Date timeStamp, Long vehicleID, float xCoord, float yCoord){

    }
    @Override
    @Async
    public CompletableFuture<List<Long>> getFreeLocks(Long stationID)throws Exception{
        try {
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/stations/free_locks/" + stationID.toString()))
                    .GET()
                    .build();



            CompletableFuture<List<Long>> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> { //System.out.println("Requested unlockStationVehicle and got response:" +response.body());
                        return response; } )
                    .thenApply(HttpResponse::body)
                    .thenApply(response -> gson.fromJson(response,new TypeToken<List<Long>>(){}.getType()));
            return CompletableFuture.completedFuture(future.get());

        }catch (Exception e){
            throw  new Exception("Ride Service is not available");
        }
    }
    @Override
    @Async
    public void lockStationVehicle(Long userID, Long stationID)throws Exception{
        try {
            VehicleStateDTO dto = new VehicleStateDTO();
            dto.setUserID(userID);
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/stations/lock/" + stationID.toString()))
                    .PUT(HttpRequest.BodyPublishers.ofString(gson.toJson(userID)))
                    .build();

            client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).get();

        }catch (Exception e){
            throw  new Exception("Ride Service is not available");
        }
    }
    @Override
    @Async
    public CompletableFuture<Boolean> lockFreeVehicle(Long userID, Long vehicleID) throws Exception{
        try {
            VehicleStateDTO dto = new VehicleStateDTO();
            dto.setUserID(userID);
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/free_vehicles/lock/" + vehicleID.toString()))
                    .PUT(HttpRequest.BodyPublishers.ofString(gson.toJson(userID)))
                    .build();

            CompletableFuture<Boolean> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> { //System.out.println("Requested unlockStationVehicle and got response:" +response.body());
                        return response; } )
                    .thenApply(HttpResponse::body)
                    .thenApply(response -> gson.fromJson(response,Boolean.TYPE));
            return CompletableFuture.completedFuture(future.get());

        }catch (Exception e){
            throw  new Exception("Ride Service is not available");
        }
    }
    @Override
    @Async
    public CompletableFuture<VehicleDTO> findNearestFreeVehicle(float xCoord, float yCoord, VehicleType vehicleType) throws Exception{
        try {
            VehicleDTO dto = new VehicleDTO();
            dto.setxCoord(xCoord);
            dto.setyCoord(yCoord);
            dto.setType(vehicleType);
            HttpRequest request = baseJsonRequest()
                    .uri(URI.create(API_URL+"/vehicles/find_nearest"))
                    .method("GET", HttpRequest.BodyPublishers.ofString(gson.toJson(dto)))
                    .build();

            CompletableFuture<VehicleDTO> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(response -> { //System.out.println("Requested unlockStationVehicle and got response:" +response.body());
                        return response; } )
                    .thenApply(HttpResponse::body)
                    .thenApply(response -> gson.fromJson(response,dto.getClass()));
            return CompletableFuture.completedFuture(future.get());

        }catch (Exception e){
           throw  new Exception("Ride Service is not available");
        }
    }

    private HttpRequest.Builder baseJsonRequest(){
        return HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .timeout(Duration.ofMillis(5000L));
    }
}
