package be.kdg.sa.simulator.ride.FileReading.EventHandler;

import be.kdg.sa.simulator.ride.FileReading.RideEvents;
import be.kdg.sa.simulator.ride.services.IRideService;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class FreeVehicleEventHandler implements IRideEventHandler {
    private Long vehicleId;
    private Long userId;
    private Date date;
    private float xCoord;
    private float yCoord;
    private Long delay;
    private final IRideService rideService;
    public FreeVehicleEventHandler(IRideService rideService){
        this.rideService = rideService;
    }

    private void mapEventData(RideEvents event, String[] eventData) throws Exception{
        try {
            if (event == RideEvents.LOCK || event == RideEvents.UNLOCK) {
                userId = Long.parseLong(eventData[0]);
                vehicleId = Long.parseLong(eventData[1]);
                delay = Long.parseLong(eventData[2]);
            } else if (event == RideEvents.SEND_LOCATION) {
                date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(eventData[0]);
                vehicleId = Long.parseLong(eventData[1]);
                xCoord = Float.parseFloat(eventData[2]);
                yCoord = Float.parseFloat(eventData[3]);
                delay = Long.parseLong(eventData[4]);
            }
        }catch (Exception e){
            throw new Exception();
        }
    }
    @Override
    public void doEvent(RideEvents event, String[] eventData) throws Exception{
        mapEventData(event,eventData);
        try {
            Thread.sleep(delay);
            if (event == RideEvents.LOCK){
                    rideService.lockFreeVehicle(userId,vehicleId);
            }else if (event == RideEvents.UNLOCK){
                    rideService.unlockFreeVehicle(userId,vehicleId);
                }else if (event == RideEvents.SEND_LOCATION){
                    rideService.sendLocationOfFreeVehicle(date,vehicleId,xCoord,yCoord);
                }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
