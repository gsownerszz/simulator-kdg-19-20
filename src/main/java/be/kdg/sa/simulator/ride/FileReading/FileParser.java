package be.kdg.sa.simulator.ride.FileReading;

import be.kdg.sa.simulator.ride.FileReading.EventHandler.FreeVehicleEventHandler;
import be.kdg.sa.simulator.ride.FileReading.EventHandler.RideEventHandler;
import be.kdg.sa.simulator.ride.FileReading.EventHandler.StationVehicleEventHandler;
import be.kdg.sa.simulator.ride.services.IRideService;
import be.kdg.sa.simulator.ride.vehicle.VehicleType;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

@Component
public class FileParser {
    private final RideEventHandler rideEventHandler;
    private final IRideService rideService;
    public FileParser(RideEventHandler rideEventHandler, IRideService rideService){
        this.rideEventHandler = rideEventHandler;
        this.rideService = rideService;
    }
    public void readFile(File file){
        BufferedReader reader;
        try {

            reader = new BufferedReader(new FileReader(
                    file));
            String line = reader.readLine();
            VehicleType type = VehicleType.valueOf(line);
            if (type == VehicleType.VELO_BIKE || type == VehicleType.VELO_E_BIKE){
                rideEventHandler.setRideEventHandler(new StationVehicleEventHandler(rideService));
            }else if (type == VehicleType.ROAMING_E_STEP || type == VehicleType.ROAMING_SCOOTER){
                rideEventHandler.setRideEventHandler(new FreeVehicleEventHandler(rideService));
            }
            line = reader.readLine();

                while (line != null) {
                    parseLine(line);
                    // read next line
                    line = reader.readLine();
                }
            reader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private void parseLine(String line) throws Exception{
        try {
            String eventIdentifier = line.split(";")[0];
            String[] eventData = line.split(";")[1].split(",");
            RideEvents event  = RideEvents.valueOf(eventIdentifier);
            rideEventHandler.doEvent(event,eventData);
        }catch (Exception e){
            throw new Exception("Something went wrong when reading the file. Make sure the file is in the correct format");
        }
    }

    private void doEvent(VehicleType type, RideEvents event, String firstData, String secondData, String delay){


    }
}
