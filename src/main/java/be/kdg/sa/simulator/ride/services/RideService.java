package be.kdg.sa.simulator.ride.services;

import be.kdg.sa.simulator.ride.REST.IRideMessageHandler;
import be.kdg.sa.simulator.ride.vehicle.VehicleDTO;
import be.kdg.sa.simulator.ride.vehicle.VehicleType;
import org.springframework.scheduling.annotation.Async;

import java.util.Date;
import java.util.List;


public class RideService implements IRideService {
    private final IRideMessageHandler messageHandler;
    public RideService(IRideMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public Long unlockStationVehicle(Long userID, Long stationID) throws Exception {
        try {
            Long lockID = messageHandler.unlockStationVehicle(userID, stationID).get();
            System.out.println("Requested unlockStationVehicle and got response:" + lockID);
            return lockID ;
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }

    }
    @Override
    public Boolean unlockFreeVehicle(Long userID, Long vehicleID) throws Exception {
        try {
            Boolean unlock = messageHandler.unlockFreeVehicle(userID, vehicleID).get();
            System.out.println("Requested unlockFreeVehicle and got response:" + unlock);
            return unlock ;
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }

    }
    @Override

    public void sendLocationOfFreeVehicle(Date timeStamp, Long vehicleID, float xCoord, float yCoord) throws Exception {
        try {
            messageHandler.sendLocationOfFreeVehicle(timeStamp, vehicleID, xCoord, yCoord);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<Long> getFreeLocks(Long stationID) throws Exception {
        try {
            List<Long> freeLocks = messageHandler.getFreeLocks(stationID).get();
            System.out.println("Requested getFreeLocks and got amount:" + freeLocks.size());
            return freeLocks ;
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }

    }

    @Override
    public void lockStationVehicle(Long userID, Long stationID) throws Exception {
        try {
             messageHandler.lockStationVehicle(userID, stationID);
            System.out.println("Requested lockStationVehicle and got no errors.");
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }
    }

    @Override
    @Async
    public Boolean lockFreeVehicle(Long userID, Long vehicleID) throws Exception {
        try {
            Boolean lock = messageHandler.lockFreeVehicle(userID, vehicleID).get();
            System.out.println("Requested lockFreeVehicle and got response:" + lock);
            return lock ;
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }
    }
    @Override
    @Async
    public VehicleDTO findNearestFreeVehicle(Float xCoord, Float yCoord, VehicleType vehicleType) throws Exception {
        try {
            VehicleDTO dto = messageHandler.findNearestFreeVehicle(xCoord, yCoord, vehicleType).get();
            dto.setType(vehicleType);
            System.out.println("Requested findNearestFreeVehicle and got response:" + dto.toString());
            return dto ;
        } catch (Exception e) {
            //Log the error.
            throw new Exception(e.getMessage());
        }

    }



}
