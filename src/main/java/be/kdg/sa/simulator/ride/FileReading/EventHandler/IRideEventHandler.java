package be.kdg.sa.simulator.ride.FileReading.EventHandler;

import be.kdg.sa.simulator.ride.FileReading.RideEvents;

public interface IRideEventHandler {
     void doEvent(RideEvents event, String[] eventData) throws Exception;
}
