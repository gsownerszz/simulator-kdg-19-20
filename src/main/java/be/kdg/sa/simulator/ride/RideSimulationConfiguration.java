package be.kdg.sa.simulator.ride;

import be.kdg.sa.simulator.ride.REST.IRideMessageHandler;
import be.kdg.sa.simulator.ride.REST.RideMessageHandler;
import be.kdg.sa.simulator.ride.services.IRideService;
import be.kdg.sa.simulator.ride.services.RideService;
import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.http.HttpClient;

@Configuration
public class RideSimulationConfiguration {
    @Bean
    Gson gson(){return new Gson();}

    @Bean
    HttpClient client(){return HttpClient.newHttpClient();}

    @Bean
    IRideMessageHandler messageHandler(){return new RideMessageHandler(gson(),client());}

    @Bean
    IRideService rideService(){return new RideService(messageHandler());}
}
