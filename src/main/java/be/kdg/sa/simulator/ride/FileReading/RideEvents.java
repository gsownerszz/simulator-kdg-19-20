package be.kdg.sa.simulator.ride.FileReading;

public enum RideEvents {
    LOCK,
    UNLOCK,
    SEND_LOCATION
}
