package be.kdg.sa.simulator.ride.services;

import be.kdg.sa.simulator.ride.vehicle.VehicleDTO;
import be.kdg.sa.simulator.ride.vehicle.VehicleType;

import java.util.Date;
import java.util.List;

public interface IRideService {

    Long unlockStationVehicle(Long userID, Long stationID) throws Exception;


    Boolean unlockFreeVehicle(Long userID, Long vehicleID) throws Exception;


    void sendLocationOfFreeVehicle(Date timeStamp, Long vehicleID, float xCoord, float yCoord) throws Exception;


    List<Long> getFreeLocks(Long stationID) throws Exception;


    void lockStationVehicle(Long userID, Long stationID) throws Exception;


    Boolean lockFreeVehicle(Long userID, Long vehicleID) throws Exception;


    VehicleDTO findNearestFreeVehicle(Float xCoord, Float yCoord, VehicleType vehicleType) throws Exception;
}
