package be.kdg.sa.simulator.ride.controllers;

import be.kdg.sa.simulator.ride.FileReading.FileParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RideController {
    private final FileParser parser;
    public RideController (FileParser parser){
        this.parser = parser;
    }

    @GetMapping("/ride-simulator")
    public ModelAndView SensorSimulator() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("html/simulator/ride_simulator");
        mav.addObject("sensorGenData", parser);
        return mav;
    }


}
