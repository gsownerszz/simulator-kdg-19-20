package be.kdg.sa.simulator;

import java.util.Calendar;
import java.util.Date;
/*
To make a whole package @Deprecated (in this situation I wanted to  deprecate package greeting)
you should annotate it in the file package-info.java,
but currently IntelliJ doesn't fully support working in the package-info.java class
Source: https://youtrack.jetbrains.com/issue/IDEA-17910?_ga=2.262603595.654465416.1569711217-588081050.1568586774
*/
@Deprecated
@SuppressWarnings("we don't use this class it's just an example (later it will be deleted")
public class DemoMessage {
    private final String citroen;
    // TODO: als een datum/tijd veld nodig is, gebruik dan LocalDate(en/of Time)
    private final Date date;

    // TODO: het is niet aan de message klasse om values te bepalen voor zijn fields!
    DemoMessage() {
        this.citroen = "geel";
        this.date = Calendar.getInstance().getTime();
    }

    public String getCitroen() {
        return citroen;
    }

    public Date getDate() {
        return date;
    }
}
