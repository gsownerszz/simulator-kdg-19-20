package be.kdg.sa.simulator;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.text.SimpleDateFormat;
@Deprecated
@SuppressWarnings("we don't use this configuration class it's just an example (later it will be deleted)")
@Configuration
@EnableAsync
public class SimulatorConfiguration {
    @Bean
    ObjectMapper objectMapper() {
        @SuppressWarnings("unchecked")
        ObjectMapper mapper = new ObjectMapper();
        // TODO: LocalDate(en/of Time) geven geen mooie serialisatie "out of the box"
        //       ... beetje extra setup is nodig
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        mapper.setDateFormat(df);
        return mapper;
    }
}
