package be.kdg.sa.simulator;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Deprecated
@SuppressWarnings("we don't use this class it's just an example (later it will be deleted)")
@Component
class DemoMessageHandler {
    private final ObjectMapper mapper;

    @Autowired
    DemoMessageHandler(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    void handleMessage(DemoMessage message) {
        // TODO: queue... maybe?

        try {
            // TODO: System.out, really?
            System.out.println(mapper.writeValueAsString(message));
        } catch (Exception e) {
            // TODO: propere foutafhandeling... (dus ook geen System.err)
        }
    }
}
