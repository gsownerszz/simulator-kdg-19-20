package be.kdg.sa.simulator;

import be.kdg.sa.simulator.ride.FileReading.FileParser;
import be.kdg.sa.simulator.sensor.sensor.RandomSensorGeneration;
import be.kdg.sa.simulator.sensor.sensor.SensorData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;


@SpringBootApplication
public class SimulatorApplication implements CommandLineRunner {
	private final RandomSensorGeneration rng;
	private final FileParser parser;
	@Autowired
	public SimulatorApplication(RandomSensorGeneration rnd, FileParser parser) {
		this.rng = rnd;
		this.parser = parser;
	}

	public static void main(String[] args) {
		SpringApplication.run(SimulatorApplication.class, args);
	}

	/*
	 * TODO: het is NIET aan de 'CommandLineRunner' om
	 *  - ... nieuwe berichten te genereren
	 *  - ... te beslissen hoeveel berichten gegenereerd moeten worden
	 *  - ... te beslissen hoeveel tijd er tussen de berichten zit
	 *  - ... te slapen
	 *  - ... een verwijzing te hebben rechtstreeks naar de handler
	 *  - ... iets te doen Mdat niets te maken heeft met 'args' verwerking
	 *
	 * TODO: het is WEL aan de 'CommandLineRunner' om
	 *  - ... 'args' te verwerken (indien de opdracht dit verlangt (eigenlijk niet dus!))
	 *  - ... een simulatie-modus in gang te zetten... met 1 regel code (max!)
	 */

	@Override
	public void run(String... args) {
		parser.readFile(new File("src/main/java/be/kdg/sa/simulator/ride_simulatie/test_velo_ride"));
		parser.readFile(new File("src/main/java/be/kdg/sa/simulator/ride_simulatie/test_bad_file"));
		for (int i = 0; i < 5; i++) { // Hard-coded 10 msgs. ...
			SensorData sensorData = rng.generateSensorValue();
			System.out.println(sensorData.toString());
			try {
				Thread.sleep(500); // Hard-coded 0.1 sec. ...
			} catch (Exception e) {
				// TODO!
			}
		}
	}
}
