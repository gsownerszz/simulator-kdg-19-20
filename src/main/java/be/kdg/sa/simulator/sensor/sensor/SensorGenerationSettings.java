package be.kdg.sa.simulator.sensor.sensor;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;
import java.util.Random;

/*
This class is made for getting data from application.property file for generating SensorData
after getting the data he wil be available for another (SensorViewModel) class to get and manipulate it (get and send it to the view).
*/


@Data
@Configuration
@PropertySource("classpath:sensor.generation.properties")
@ConfigurationProperties(prefix = "sensor.default.settings")
//@PropertySource("classpath:application.properties")
public class SensorGenerationSettings {
    private final Random rnd;
    //time_period in minuten
    private int time_period;
    //delay in millisecondes
    private double delay;
    //variation in millisecondes
    private int variatie;
    //
    private Double xCoordUpperBound;
    private Double xCoordLowerBound;
    private Double yCoordUpperBound;
    private Double yCoordLowerBound;
    private List<String> sensorTypes;
    @Autowired
    public SensorGenerationSettings(final Random rnd){
    this.rnd = rnd;
    }

//getters
    public int getTime_period() {
        return time_period;
    }

    public Random getRnd() {
        return rnd;
    }

    public double getDelay() {
        return delay;
    }

    public int getVariatie() {
        return variatie;
    }

    public Double getxCoordUpperBound() {
        return xCoordUpperBound;
    }

    public Double getxCoordLowerBound() {
        return xCoordLowerBound;
    }

    public Double getyCoordUpperBound() {
        return yCoordUpperBound;
    }

    public Double getyCoordLowerBound() {
        return yCoordLowerBound;
    }

    public List<String> getSensorTypes() {
        return sensorTypes;
    }

    //setters
    public void setTime_period(int time_period) {
        this.time_period = time_period;
    }

    public void setDelay(double delay) {
        this.delay = delay;
    }

    public void setVariatie(int variatie) {
        this.variatie = variatie;
    }

    public void setxCoordUpperBound(Double xCoordUpperBound) {
        this.xCoordUpperBound = xCoordUpperBound;
    }

    public void setxCoordLowerBound(Double xCoordLowerBound) {
        this.xCoordLowerBound = xCoordLowerBound;
    }

    public void setyCoordUpperBound(Double yCoordUpperBound) {
        this.yCoordUpperBound = yCoordUpperBound;
    }

    public void setyCoordLowerBound(Double yCoordLowerBound) {
        this.yCoordLowerBound = yCoordLowerBound;
    }

    public void setSensorTypes(List<String> sensorTypes) {
        this.sensorTypes = sensorTypes;
    }
}



