package be.kdg.sa.simulator.sensor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class SensorSimulatorConfiguration {
    @Bean
    Random random() {
        return new Random();
    }
}
