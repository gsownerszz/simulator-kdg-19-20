package be.kdg.sa.simulator.sensor.sensor;


import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Data
public class SensorData {
    private double value;
    private String sensorType;
    //sensor datum word automatish ingevoeld tijdens de meting dus het kan niet handmattig toegevoegd of veranderd worden!
    private Date creationDate;
    private double xCoord;
    private double yCoord;


    public SensorData(double value, String sensorType, double xCoord, double yCoord) {
        this.value = value;
        this.creationDate = Calendar.getInstance().getTime();
        this.sensorType = sensorType;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }
    public String getSensorType() {
        return sensorType;
    }

    public double getxCoord() {
        return xCoord;
    }

    public double getyCoord() {
        return yCoord;
    }

    public double getValue() {
        return value;
    }

    public Date getDate() {
        return creationDate;
    }



    @Override
    public String toString() {
        return String.format("Sensor measurement on %s %tr in %-1g %-1g with value %1g %s",
                SimpleDateFormat.getDateInstance(3).format(this.creationDate), this.creationDate, this.xCoord, this.yCoord, this.value, this.sensorType);
    }
}
