package be.kdg.sa.simulator.sensor.sensor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RandomSensorGeneration {
    private final SensorGenerationSettings sgs;

    @Autowired
    public RandomSensorGeneration(final SensorGenerationSettings sgs) {
       this.sgs =sgs;
    }

    public SensorData generateSensorValue() {
        return new SensorData(sgs.getRnd().nextDouble() * (100D),
                sgs.getSensorTypes().get(sgs.getRnd().nextInt(3)),
                (sgs.getRnd().nextDouble() * (sgs.getxCoordUpperBound() - sgs.getxCoordLowerBound())) + sgs.getxCoordLowerBound() ,
                (sgs.getRnd().nextDouble() * (sgs.getyCoordUpperBound() - sgs.getyCoordLowerBound())) + sgs.getyCoordLowerBound() );
    }

}
