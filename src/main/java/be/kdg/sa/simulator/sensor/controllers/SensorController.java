package be.kdg.sa.simulator.sensor.controllers;

import be.kdg.sa.simulator.sensor.sensor.SensorGenerationSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SensorController {
    private final SensorGenerationSettings sgs;

    @Autowired
    SensorController(final SensorGenerationSettings sgs) {
        this.sgs = sgs;
    }
    @GetMapping({"/index", "./"})
    public String indexleg() {
        return "/index";
    }


    @GetMapping({"/sensor-simulator/*", "/*"})
    public String error404() {
        return "html/error/not_found_404";
    }

    @GetMapping("/sensor-simulator")
    public ModelAndView SensorSimulator() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("html/simulator/sensor_simulator");
        mav.addObject("sensorGenData", sgs);
        return mav;
    }


}
