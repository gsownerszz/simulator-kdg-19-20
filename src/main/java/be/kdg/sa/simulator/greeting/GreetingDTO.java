package be.kdg.sa.simulator.greeting;

public class GreetingDTO {
    private String content;

    // No args constructor is nodig vanwege gebruik met @FromBody
    public GreetingDTO() {
    }

    public GreetingDTO(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
