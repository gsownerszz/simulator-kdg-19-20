package be.kdg.sa.simulator.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GreetingController {
    private final GreetingService greetingService;

    // TODO: omzetting van en naar ViewModels doe je best met behulp van een library!

    @Autowired
    public GreetingController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping("/")
    public String welcome() {
        return "welcome_page";
    }

    @GetMapping("/details")
    public ModelAndView welcome(@RequestParam long id) {
        GreetingViewModel vm = new GreetingViewModel(
                greetingService.get(id).getContent()
        );

        ModelAndView mav = new ModelAndView();
        mav.setViewName("details_page");
        mav.addObject("greeting", vm);
        return mav;
    }

    @GetMapping("/new")
    public ModelAndView newGreeting() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("new_greeting");
        mav.addObject("greeting", new GreetingViewModel());
        return mav;
    }

    @PostMapping("/new")
    public String newGreeting(@ModelAttribute GreetingViewModel greetingVM) {
        Greeting greeting = new Greeting(greetingVM.getContent());
        greetingService.save(greeting);

        // TODO: beter doorverwijzen naar de detail-pagina! ("redirect:/pagina", RedirectAttributes, ...)
        return "hello";
    }
}
