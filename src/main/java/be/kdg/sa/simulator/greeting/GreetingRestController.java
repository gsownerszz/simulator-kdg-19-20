package be.kdg.sa.simulator.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GreetingRestController {
    private final GreetingService greetingService;

    // TODO: omzetting van en naar DTO's doe je best met behulp van een library!

    @Autowired
    public GreetingRestController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping("/greetings/{id}")
    public GreetingDTO getGreeting(@PathVariable long id) {
        Greeting greeting = greetingService.get(id);
        return new GreetingDTO(greeting.getContent());
    }

    @PostMapping("/greetings")
    public ResponseEntity<GreetingDTO> addGreeting(@RequestBody GreetingDTO greetingDTO) {
        Greeting greeting = new Greeting(greetingDTO.getContent());
        Greeting savedGreeting = greetingService.save(greeting);
        return new ResponseEntity<>(
                new GreetingDTO(savedGreeting.getContent()),
                HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/goodbye")
    public GreetingDTO goodbye() {
        return new GreetingDTO("Goodbye");
    }
}
