package be.kdg.sa.simulator.greeting;

public class GreetingViewModel {
    private String content;

    public GreetingViewModel() {
    }

    public GreetingViewModel(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
