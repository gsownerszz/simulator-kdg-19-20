package be.kdg.sa.simulator.greeting;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GreetingService {
    // TODO: eventually, store them in a DB table
    private final List<Greeting> greetings;

    public GreetingService() {
        greetings = new ArrayList<>();
    }

    public Greeting save(Greeting greeting) {
        // TODO: Geen goede ID! Taak van de DB!
        greeting.setId(greetings.size() + 1);

        greetings.add(greeting);

        // TODO: 'System.out' ? ...
        System.out.println("Saved: " + greeting);

        return greeting;
    }

    public Greeting get(long id) {
        return greetings
                .stream()
                .filter(greeting -> greeting.getId() == id)
                .findFirst()
                .orElse(null);
    }
}
